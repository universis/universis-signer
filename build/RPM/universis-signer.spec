%global provider gitlab
%global provider_tld com
%global project universis
%global repo %{name}
%global import_path %{provider}.%{provider_tld}/%{project}/%{repo}
%global harica_import_path %{provider}.%{provider_tld}/%{project}/%{repo}-harica-client
%global git0 https://%{import_path}
%global git1 https://%{harica_import_path}
%global commit0 ${CI_COMMIT_SHA}
%global commit1 1b1801cc896bd252a633e244d0842f737510567f
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global shortcommit0 %(c=%{commit1}; echo ${c:0:7})
%global version 1.8.1
%global debug_package %{nil}

Name:     universis-signer
Version:  %{version}.2
Release:  1%{?dist}
Summary:  Digitally sign files using your academic id or certificate
Source0:  %{git0}/-/archive/%{commit0}/%{name}-%{commit0}.tar.gz
Source1:  %{git1}/-/archive/%{commit1}/%{name}-harica-client-%{commit1}.tar.gz
License:  LGPL v3
URL:      https://gitlab.com/universis/universis-signer
Requires: java-1.8.0-openjdk libclassicclient libusb-devel libusb pcsc-lite-ccid opensc pcsc-lite pcsc-lite-devel
BuildRequires: systemd-rpm-macros
BuildRequires: java-1.8.0-openjdk
BuildRequires: maven-local
BuildRoot: %{RPM_BUILD_ROOT}

%description
Digitally sign your files using your academic id or certificate

%prep
%setup -b 1 -n %{name}-harica-client-%{commit1}
# this is used to change the directory harica client will use for the jar. This would be a chore to maintain if we used a patch
sed -i -r -e "s|${basedir}/../%{name}/|${basedir}/../%{name}-%{commit0}/|g" pom.xml
cd ../
%setup -n %{name}-%{commit0}
exit

%build
mvn package -Dmaven.test.skip=true
cd ../%{name}-harica-client-%{commit1}
mvn package -Dmaven.test.skip=true
cp target/universis-signer-harica.jar ../%{name}-%{commit0}/target/
exit

%install
mkdir -p $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/
mkdir -p $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/extras
mkdir -p $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/extras/linux
mkdir -p $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/lib
mkdir -p $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/log
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system/
cp -r $(pwd)/target/extras/linux/* $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/extras/linux
cp -r $(pwd)/target/lib/* $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/lib/
cp -r $(pwd)/target/universis-signer.jar $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/
cp -r $(pwd)/target/universis-signer-harica.jar $RPM_BUILD_ROOT/opt/universis-signer-linux-x64/
cp $(pwd)/target/extras/linux/universis-signer.service $RPM_BUILD_ROOT/usr/lib/systemd/system/universis-signer.service

exit

%post
# copy service
sed -i -r -e 's|SERVICE_PATH/jre/bin/java|$JAVA_HOME/bin/java|g' /opt/universis-signer-linux-x64/extras/linux/universis-signer.service
sed -i -r -e 's|SERVICE_PATH|/opt/universis-signer-linux-x64|g' /opt/universis-signer-linux-x64/extras/linux/universis-signer.service
cp /opt/universis-signer-linux-x64/extras/linux/universis-signer.service /usr/lib/systemd/system/universis-signer.service
chmod 700 /opt/universis-signer-linux-x64/
chmod 644 /usr/lib/systemd/system/universis-signer.service
chown root:root /usr/lib/systemd/system/universis-signer.service
# create symlink for eToken lib
[ -f /usr/lib64/libeToken.so ] && [ ! -f "/usr/lib/libeToken.so" ] && ln -s /usr/lib64/libeToken.so /usr/lib/libeToken.so
%systemd_post universis-signer.service
systemctl daemon-reload
systemctl enable universis-signer.service
systemctl start universis-signer.service

%files
%attr(0644, root, root) /opt/universis-signer-linux-x64/extras/*
%config(noreplace) /opt/universis-signer-linux-x64/extras/linux/eToken.cfg
%attr(0644, root, root) /opt/universis-signer-linux-x64/lib/*.jar
%attr(0644, root, root) /opt/universis-signer-linux-x64/universis-signer.jar
%attr(0644, root, root) /opt/universis-signer-linux-x64/universis-signer-harica.jar
%attr(0644, root, root) /usr/lib/systemd/system/universis-signer.service


%preun
%systemd_preun universis-signer.service
systemctl daemon-reload
systemctl reset-failed

%postun
%systemd_postun_with_restart universis-signer.service

%changelog
* Wed Jun 23 2021 UniverSIS Maintainers <no-reply@universis.io> - 1.8.1
- Package with universis-signer with the optional harica-client
* Mon May 24 2021 UniverSIS Maintainers <no-reply@universis.io> - 1.8.1
- update to 1.8.1
