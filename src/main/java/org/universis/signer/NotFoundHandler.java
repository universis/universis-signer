package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 404 Not Found error
 */
public class NotFoundHandler extends ErrorHandler {
    public NotFoundHandler() {
        super(NanoHTTPD.Response.Status.NOT_FOUND);
    }
    public NotFoundHandler(Exception e) {
        super(e);
        this.status = NanoHTTPD.Response.Status.NOT_FOUND;
    }
}
