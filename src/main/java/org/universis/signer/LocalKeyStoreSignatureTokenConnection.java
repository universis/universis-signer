package org.universis.signer;

import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.token.AbstractKeyStoreTokenConnection;

import javax.security.auth.DestroyFailedException;
import java.security.KeyStore;

public class LocalKeyStoreSignatureTokenConnection extends AbstractKeyStoreTokenConnection {

    public LocalKeyStoreSignatureTokenConnection(KeyStore ks, KeyStore.PasswordProtection ksPassword) {
        this.keyStore = ks;
        this.password = ksPassword;
    }

    /** The KeyStore */
    private final KeyStore keyStore;

    /** The password for the KeyStore */
    private final KeyStore.PasswordProtection password;


    @Override
    protected KeyStore getKeyStore() throws DSSException {
        return this.keyStore;
    }

    @Override
    protected KeyStore.PasswordProtection getKeyProtectionParameter() {
        return this.password;
    }

    @Override
    public void close() {
        if (password != null) {
            try {
                password.destroy();
            } catch (DestroyFailedException e) {
                //
            }
        }
    }
}
