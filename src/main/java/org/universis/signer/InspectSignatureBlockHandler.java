package org.universis.signer;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.router.RouterNanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResponder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles /sign requests for signing pdf documents
 */
public class InspectSignatureBlockHandler implements UriResponder {
    private static final Logger log = LogManager.getLogger(InspectSignatureBlockHandler.class);

    public Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {

        Map<String, String> files = new HashMap<String, String>();

        try {
            ihttpSession.parseBody(files);
            Map<String, List<String>> params = ihttpSession.getParameters();
            for(String file : files.values())
            {
                // read and sign file
            }
            // get pdf file
            if (!files.containsKey("file")) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            String inFile = files.get("file");
            SignatureBlockConfiguration blockConfiguration = new SignatureBlockConfiguration();
            // get text to search for
            if (params.containsKey("text")) {
                params.get("text").forEach((value) -> {
                    try {
                        blockConfiguration.signatureBlockText.add(URLDecoder.decode(value, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                blockConfiguration.signatureBlockText.add("(digital signature)");
            }
            // get text position in block
            if (params.containsKey("textPosition")) {
                blockConfiguration.signatureBlockTextPosition = params.get("textPosition").get(0);
            }
            // get block width
            if (params.containsKey("width")) {
                blockConfiguration.signatureBlockWidth = Integer.parseInt(params.get("width").get(0));
            }
            // get block height
            if (params.containsKey("height")) {
                blockConfiguration.signatureBlockHeight = Integer.parseInt(params.get("height").get(0));
            }
            // open filed
            PdfReader reader = new PdfReader(inFile);
            // init position listener
            SignaturePositionListener listener = new SignaturePositionListener();
            //set block configuration to search for
            listener.configuration = blockConfiguration;
            PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
            // get number of pages
            int numberOfPages = reader.getNumberOfPages();
            InspectSignatureBlockResult result = new InspectSignatureBlockResult();
            // and loop to find position
            for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
                PdfDictionary pageDic = reader.getPageN(pageNum);
                PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
                // process content
                processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
                if (listener.position != null) {
                    result.position = new int[] {
                            (int)listener.position.getLeft(),
                            (int)listener.position.getBottom(),
                            (int)listener.position.getLeft() + (int)listener.position.getWidth(),
                            (int)listener.position.getBottom() + (int)listener.position.getHeight()
                    };
                    result.page = pageNum;
                    break;
                }
            }
            NanoHTTPD.Response res = new JsonResponseHandler((Serializable) result).get(uriResource, map, ihttpSession);

            CorsHandler.enable(res);
            // set client origin if any
            Map<String, String> headers = ihttpSession.getHeaders();
            if (headers.containsKey("origin")) {
                res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
            }
            // and return
            return res;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    public Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
