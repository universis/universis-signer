package org.universis.signer;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.security.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

/**
 * Signs pdf documents
 */
public class Signer {

    private KeyStore _keyStore;

    public Signer(KeyStore keyStore) {
        this._keyStore = keyStore;
    }

    public Signer() {
    }

    /**
     * Signs the given input stream which represents a pdf document
     * @param inputStream - The input stream
     * @param outputStream - The output stream
     * @param thumbprint - The thumbprint of the certificate that is going to used for signing
     * @param password - The password of the private key that is going to used for signing
     * @param reason - A string which contains the reason of pdf signing, normally a description of a document. This string will be visible in pdf signature block
     * @param position - Defines the position of the signature
     * @param timestampServer - Defines a timestamp server that is going to be used during signing
     * @param name - A string which which represents the signature field name
     * @param image - A string which which represents the path of an image to embedded in signature
     */
    public void sign(InputStream inputStream,
                     OutputStream outputStream,
                     String thumbprint,
                     String password,
                     String reason,
                     int page,
                     Rectangle position,
                     String timestampServer,
                     String name,
                     String image) throws IOException, DocumentException, GeneralSecurityException {
        // create reader
        PdfReader reader = new PdfReader(inputStream);
        // create signature
        PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\0', null, true);
        // get signature appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        if (reason != null) {
            appearance.setReason(reason);
        }
        Font font = new Font(Font.FontFamily.HELVETICA, 10);
        appearance.setLayer2Font(font);

        // set signature position
        Rectangle finalPosition = new Rectangle(50, 10, 290, 100);
        if (position != null) {
            finalPosition = position;
        }
        // add image
        if (image != null) {
            Image imageInstance = Image.getInstance(image);
            appearance.setSignatureGraphic(imageInstance);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);
        }
        appearance.setVisibleSignature(finalPosition, page, name == null ? "sig": name);
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        String providerName = provider.getName();
        // open keystore and sign
        KeyStore ks = this._keyStore;
        // get keystore aliases
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias = null;
        String findAlias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint.toLowerCase())) {
                    findAlias = alias;
                    break;
                }
            }
        }
        if (findAlias == null) {
            throw new CertificateException("The specified certificate cannot be found");
        }
        // get private key
        final Key key = ks.getKey(alias, password.toCharArray());
        // get cert
        final Certificate[] chain = ks.getCertificateChain(alias);
        // create digest algorithm
        PrivateKeySignature pks = null;
        // pkcs11 signature
        if (ks.getType().toLowerCase().equals("pkcs11")) {
            pks = new PrivateKeySignature((PrivateKey) key, DigestAlgorithms.SHA256, ks.getProvider().getName());
        } else {
            // pkcs12 signature
            pks = new PrivateKeySignature((PrivateKey) key, DigestAlgorithms.SHA256, providerName);
        }
        BouncyCastleDigest digest = new BouncyCastleDigest();
        TSAClientBouncyCastle tsaClient = null;
        if (timestampServer != null) {
            tsaClient = new TSAClientBouncyCastle(timestampServer, "", "");
        }
        // and finally sign pdf
        MakeSignature.signDetached(appearance, digest, pks, chain, null, null, tsaClient, 0, MakeSignature.CryptoStandard.CMS);



    }

    /**
     * Signs the given pdf document
     * @param inFile - An absolute path of the input document
     * @param outFile - An absolute path of the output document
     * @param thumbprint - The thumbprint of the certificate that is going to used for signing
     * @param password - The password of the private key that is going to used for signing
     * @param reason - A string which contains the reason of pdf signing, normally a description of a document. This string will be visible in pdf signature block
     * @param position - Defines the position of the signature
     * @param timestampServer - Defines a timestamp server that is going to be used during signing
     */
    public void sign(String inFile, String outFile, String thumbprint, String password, String reason, int page, Rectangle position, String timestampServer) throws IOException, DocumentException, GeneralSecurityException {
        this.sign(new FileInputStream(inFile), new FileOutputStream(outFile), thumbprint, password, reason, page, position, timestampServer, null, null);
    }

    /**
     * Signs the given pdf document
     * @param inFile - An absolute path of the input document
     * @param outFile - An absolute path of the output document
     * @param thumbprint - The thumbprint of the certificate that is going to used for signing
     * @param password - The password of the private key that is going to used for signing
     * @param reason - A string which contains the reason of pdf signing, normally a description of a document. This string will be visible in pdf signature block
     * @param position - Defines the position of the signature
     * @param timestampServer - Defines a timestamp server that is going to be used during signing
     * @param name - A string which which represents the signature field name
     * @param imageFile - A string which which represents the path of an image to embedded in signature
     */
    public void sign(String inFile, String outFile, String thumbprint, String password, String reason, int page, Rectangle position, String timestampServer, String name, String imageFile) throws IOException, DocumentException, GeneralSecurityException {
        this.sign(new FileInputStream(inFile), new FileOutputStream(outFile), thumbprint, password, reason, page, position, timestampServer, name, imageFile);
    }

    public List<VerifySignatureResult> verify(File inFile) throws InvalidFormatException, IOException, GeneralSecurityException {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        String providerName = provider.getName();
        // create reader
        PdfReader pdfReader = new PdfReader(new FileInputStream(inFile));
        AcroFields acroFields = pdfReader.getAcroFields();
        List<String> signatureNames = acroFields.getSignatureNames();
        List<VerifySignatureResult> results = new ArrayList<>();
        if (!signatureNames.isEmpty()) {
            for (String name : signatureNames) {
                if (acroFields.signatureCoversWholeDocument(name)) {
                    VerifySignatureResult result = new VerifySignatureResult();
                    Security.getProviders();
                    PdfPKCS7 pkcs7 = acroFields.verifySignature(name, providerName);
                    result.valid = pkcs7.verify();
                    String reason = pkcs7.getReason();
                    Calendar signedAt = pkcs7.getSignDate();
                    X509Certificate signingCertificate = pkcs7.getSigningCertificate();
                    // add certificates
                    result.certificates = new ArrayList<X509CertificateInfo>();
                    result.certificates.add(new X509CertificateInfo(signingCertificate));
                    // add signature properties
                    result.signatureProperties = new SignatureProperties();
                    // reason
                    result.signatureProperties.reason = reason;
                    // signing certificate
                    result.signatureProperties.signingCertificate = new X509CertificateInfo(signingCertificate);
                    // signing date
                    result.signatureProperties.signingDate = signedAt.getTime();
                    // add to list
                    results.add(result);
                }
            }
        }
        return results;
    }

}
