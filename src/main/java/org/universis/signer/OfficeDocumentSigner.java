package org.universis.signer;

import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import org.apache.commons.io.FileUtils;
import org.apache.jcp.xml.dsig.internal.dom.DOMSignedInfo;
import org.apache.jcp.xml.dsig.internal.dom.DOMSubTreeData;
import org.apache.poi.ooxml.util.DocumentHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.apache.poi.poifs.crypt.dsig.SignaturePart;
import org.apache.poi.poifs.crypt.dsig.facets.KeyInfoSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.OOXMLSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.Office2010SignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.XAdESSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.services.RevocationData;
import org.apache.poi.poifs.crypt.dsig.services.TimeStampService;
import org.apache.poi.util.LocaleUtil;
import org.apache.xmlbeans.XmlException;
import org.w3.x2000.x09.xmldsig.KeyInfoType;
import org.w3.x2000.x09.xmldsig.SignatureDocument;
import org.w3.x2000.x09.xmldsig.SignatureType;
import org.w3.x2000.x09.xmldsig.X509DataType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sun.security.pkcs11.SunPKCS11;
import sun.security.pkcs11.wrapper.CK_MECHANISM_INFO;
import sun.security.pkcs11.wrapper.PKCS11Constants;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.lang.reflect.Field;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class OfficeDocumentSigner {

    private KeyStore _keyStore = null;

    public OfficeDocumentSigner() {
    }
    
    public OfficeDocumentSigner(KeyStore keyStore) {
        this._keyStore = keyStore;
    }

    private Element getDsigElement(Document document, String localName) {
        NodeList sigValNl = document.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", localName);
        if (sigValNl.getLength() == 1) {
            return (Element)sigValNl.item(0);
        } else {
           return null;
        }
    }

    public static void tryFixingPKCS11ProviderBug(SunPKCS11 provider) {
        try {
            Field tokenField = SunPKCS11.class.getDeclaredField("token");
            tokenField.setAccessible(true);
            Object token = tokenField.get(provider);

            Field mechInfoMapField = token.getClass().getDeclaredField("mechInfoMap");
            mechInfoMapField.setAccessible(true);
            @SuppressWarnings("unchecked")
            Map<Long, CK_MECHANISM_INFO> mechInfoMap = (Map<Long, CK_MECHANISM_INFO>) mechInfoMapField.get(token);
            mechInfoMap.put(PKCS11Constants.CKM_SHA1_RSA_PKCS, new CK_MECHANISM_INFO(1024, 2048, 0));
        } catch(Exception e) {
            System.out.printf("Method tryFixingPKCS11ProviderBug failed with '%s'%n", e.getMessage());
        }
    }

    private Certificate[] getCertificateChain(KeyStore ks, String thumbprint) throws KeyStoreException, CertificateException, NoSuchAlgorithmException {
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias = null;
        String findAlias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint.toLowerCase())) {
                    findAlias = alias;
                    break;
                }
            }
        }
        if (findAlias == null) {
            throw new CertificateException("The specified certificate cannot be found");
        }
        return ks.getCertificateChain(alias);
    }

    private PrivateKey getPrivateKey(KeyStore ks, String thumbprint,
                          String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias = null;
        String findAlias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint.toLowerCase())) {
                    findAlias = alias;
                    break;
                }
            }
        }
        if (findAlias == null) {
            throw new CertificateException("The specified certificate cannot be found");
        }
        // get private key
        return (PrivateKey) ks.getKey(alias, password.toCharArray());
    }

    public void sign(File inFile,
                     File outFile,
         String thumbprint,
         String password,
         String reason,
         String timestampServer) throws GeneralSecurityException, IOException, InvalidFormatException, XMLSignatureException, MarshalException, TransformException, NoSuchFieldException {
        // open keystore and sign
        if (this._keyStore == null) {
            throw new KeyStoreException("Key store cannot be empty at this context");
        }
        KeyStore ks = this._keyStore;
        // get private key
        PrivateKey privateKey = this.getPrivateKey (ks, thumbprint, password);
        // get cert
        Certificate[] chain = this.getCertificateChain(ks, thumbprint);
        // copy file
        if (!inFile.equals(outFile)) {
            FileUtils.copyFile(inFile, outFile);
        }
        // open package
        OPCPackage pkg = OPCPackage.open(outFile, PackageAccess.READ_WRITE);
        // create signature configuration
        SignatureConfig signatureConfig = new SignatureConfig();
        // if timestamp server is not defined
        if (timestampServer == null) {
            TimeStampService tspService = new TimeStampService() {
                @Override
                public byte[] timeStamp(byte[] data, RevocationData revocationData) throws Exception {
                    return "time-stamp-token".getBytes(LocaleUtil.CHARSET_1252);
                }
                @Override
                public void setSignatureConfig(SignatureConfig config) {
                    // empty on purpose
                }
            };
            signatureConfig.setTspService(tspService);
        } else {
            // set timestamp server url
            signatureConfig.setTspUrl(timestampServer);
        }
        // set no request policy for tsp
        signatureConfig.setTspRequestPolicy(null);
        // set digest algorithm
        signatureConfig.setDigestAlgo(HashAlgorithm.sha1);
        // create certificate chain
        List<X509Certificate> certificateChain = new ArrayList<>();
        for (Certificate cert : chain) {
            certificateChain.add((X509Certificate) cert);
        }
        // set certificate chain
        signatureConfig.setSigningCertificateChain(certificateChain);

        // set private key
        signatureConfig.setKey(privateKey);
        // set description
        if (reason != null) {
            signatureConfig.setSignatureDescription("Document signature");
        }
        // add signature facets
        signatureConfig.addSignatureFacet(new OOXMLSignatureFacet());
        signatureConfig.addSignatureFacet(new KeyInfoSignatureFacet());
        signatureConfig.addSignatureFacet(new XAdESSignatureFacet());
        signatureConfig.addSignatureFacet(new Office2010SignatureFacet());
        // include certificate chain
        signatureConfig.setIncludeEntireCertificateChain(true);
        // set package
        signatureConfig.setOpcPackage(pkg);
        // perform sign
        SignatureInfo si = new SignatureInfo();
        si.setSignatureConfig(signatureConfig);

        Document document = DocumentHelper.createDocument();
        DOMSignContext xmlSignContext = si.createXMLSignContext(document);
        xmlSignContext.setProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider", ks.getProvider());
        // call pre-sign operation
        DOMSignedInfo signedInfo = si.preSign(xmlSignContext);
        // get signedInfo element
        if (ks.getProvider() instanceof SunPKCS11) {
            Document doc = (Document)xmlSignContext.getParent();
            Element el = this.getDsigElement(doc, "SignedInfo");
            DOMSubTreeData subTree = new DOMSubTreeData(el, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // get element as stream
            signedInfo.getCanonicalizationMethod().transform(subTree, xmlSignContext, stream);
            try {
                // create signature value
                PrivateKeySignature pks = new PrivateKeySignature(privateKey, DigestAlgorithms.SHA1, ks.getProvider().getName());
                // sign SignedInfo element
                byte[] res = pks.sign(stream.toByteArray());
                // get base64 value
                String signatureValue = Base64.getEncoder().encodeToString(res);
                // call post-sign operation
                si.postSign(xmlSignContext, signatureValue);
            } catch (GeneralSecurityException e) {
                throw e;
            }
        } else {
            // call sign digest operation
            String signatureValue = si.signDigest(xmlSignContext, signedInfo);
            // call post-sign operation
            si.postSign(xmlSignContext, signatureValue);
        }
        // and close document
        pkg.close();
    }

    public VerifySignatureResult verify(File inFile) throws InvalidFormatException {
        // open package
        OPCPackage pkg = OPCPackage.open(inFile, PackageAccess.READ);
        SignatureConfig signatureConfig = new SignatureConfig();
        signatureConfig.addSignatureFacet(new OOXMLSignatureFacet());
        signatureConfig.addSignatureFacet(new KeyInfoSignatureFacet());
        signatureConfig.addSignatureFacet(new XAdESSignatureFacet());
        signatureConfig.addSignatureFacet(new Office2010SignatureFacet());
        // set package
        signatureConfig.setOpcPackage(pkg);
        // perform sign
        SignatureInfo si = new SignatureInfo();
        si.setSignatureConfig(signatureConfig);


        VerifySignatureResult result = new VerifySignatureResult();
        result.signatureProperties = new SignatureProperties();
        result.valid = si.verifySignature();
        result.certificates = new ArrayList<>();
        for (SignaturePart signaturePart : si.getSignatureParts()) {
            try {
                SignatureDocument signatureDocument = signaturePart.getSignatureDocument();
                SignatureType signature = signatureDocument.getSignature();
                KeyInfoType keyInfo = signature.getKeyInfo();
                // get certificates
                List<X509DataType> x509DataList = keyInfo.getX509DataList();
                x509DataList.forEach(x509DataType -> {
                    byte[][] x509CertificateArray = x509DataType.getX509CertificateArray();
                    CertificateFactory certFactory = null;
                    try {
                        certFactory = CertificateFactory.getInstance("X.509");
                        for (byte[] bytes : x509CertificateArray) {
                            InputStream in = new ByteArrayInputStream(bytes);
                            X509Certificate cert = (X509Certificate) certFactory.generateCertificate(in);
                            X509CertificateInfo info = new X509CertificateInfo(cert);
                            result.certificates.add(info);
                        }
                    } catch (CertificateException | NoSuchAlgorithmException | IOException e) {
                        e.printStackTrace();
                    }
                });

                NamespaceContext ctx = new NamespaceContext() {
                    public String getNamespaceURI(String prefix) {
                        if (prefix.equals("xd")) {
                            return "http://uri.etsi.org/01903/v1.3.2#";
                        } else if (prefix.equals("ns0")) {
                            return "http://www.w3.org/2000/09/xmldsig#";
                        }
                        return null;
                    }
                    public Iterator getPrefixes(String val) {
                        return null;
                    }

                    public String getPrefix(String uri) {
                        return null;
                    }
                };

                // get signature properties from node
                XPath xPath = XPathFactory.newInstance().newXPath();
                xPath.setNamespaceContext(ctx);
                try {
                    Node signedSignaturePropertiesNode = (Node) xPath.compile("//xd:QualifyingProperties/xd:SignedProperties/xd:SignedSignatureProperties")
                            .evaluate(signature.getDomNode(), XPathConstants.NODE);
                    if (signedSignaturePropertiesNode != null) {
                        // get signing time
                        Node signingTimeNode = (Node) xPath.compile("./xd:SigningTime")
                                .evaluate(signedSignaturePropertiesNode, XPathConstants.NODE);
                        if (signingTimeNode != null) {
                            if (signingTimeNode.getChildNodes().getLength() > 0) {
                                String text = signingTimeNode.getChildNodes().item(0).getNodeValue();
                                if (!text.endsWith("Z")) {
                                    text += "Z";
                                }
                                result.signatureProperties.signingDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(text);
                            }
                        }
                        // get signing cert serial
                        Node serialNumberNode = (Node) xPath.compile("./xd:SigningCertificate/xd:Cert/xd:IssuerSerial/ns0:X509SerialNumber")
                                .evaluate(signedSignaturePropertiesNode, XPathConstants.NODE);

                        if (serialNumberNode != null) {
                            String serialNumber = serialNumberNode.getChildNodes().item(0).getNodeValue();
                            result.certificates.forEach(x509CertificateInfo -> {
                                 if (x509CertificateInfo.serialNumber.toString().equals(serialNumber)) {
                                     result.signatureProperties.signingCertificate = x509CertificateInfo;
                                 }
                            });
                        }

                    }
                } catch (XPathExpressionException | ParseException e) {
                    e.printStackTrace();
                }



            } catch (IOException | XmlException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public void embedSignatureLine(File inFile, File outFile) throws InvalidFormatException, IOException {

        OPCPackage pkg = OPCPackage.open(inFile, PackageAccess.READ_WRITE);

        // get parts
        ArrayList<PackagePart> parts = pkg.getParts();
        // try to find vml drawing and remove it
        for (PackagePart part: parts) {
            if (part.getPartName().getName().equals("/xl/drawings/vmlDrawing1.vml")) {
                pkg.removePart(part);
                break;
            }
        }
        // try to find media image1 and remove it
        for (PackagePart part: parts) {
            if (part.getPartName().getName().equals("/xl/media/image1.emf")) {
                pkg.removePart(part);
                break;
            }
        }

        PackagePartName vmlDrawing1Name = PackagingURIHelper.createPartName("/xl/drawings/vmlDrawing1.vml");
        PackagePart vmlDrawing1 = pkg.createPart(vmlDrawing1Name, "application/vnd.openxmlformats-officedocument.vmlDrawing");
        // get vml drawing template
        vmlDrawing1.load(OfficeDocumentSigner.class.getResourceAsStream("/org/universis/signer/signatureLine/drawings/vmlDrawing1.vml"));
        // get emf image template
        PackagePartName image1Name = PackagingURIHelper.createPartName("/xl/media/image1.emf");
        PackagePart image1 = pkg.createPart(image1Name, "image/x-emf");
        image1.load(OfficeDocumentSigner.class.getResourceAsStream("/org/universis/signer/signatureLine/media/image1.emf"));

        // create relationship between drawing and image
        vmlDrawing1.addRelationship(image1Name, TargetMode.INTERNAL, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image");
        // try to find the first spreadsheet
        parts = pkg.getParts();
        // find first spreadsheet
        PackagePart findPart = null;
        for (PackagePart part: parts) {
            if (part.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml")) {
                findPart = part;
                break;
            }
        }
        if (findPart != null) {
            // create relationship between spreadsheet and drawing
            findPart.addRelationship(vmlDrawing1Name, TargetMode.INTERNAL, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing");
        } else {
            // or otherwise throw exception
            throw new IOException("Document spreadsheet cannot be found");
        }
        // and finally save output
        pkg.save(outFile);
    }

}
