package org.universis.signer;

import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.enumerations.JWSSerializationType;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.enumerations.SignaturePackaging;
import eu.europa.esig.dss.jades.JAdESSignatureParameters;
import eu.europa.esig.dss.jades.signature.JAdESService;
import eu.europa.esig.dss.jades.validation.JWSSerializationDocumentValidator;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.model.x509.CertificateToken;
import eu.europa.esig.dss.service.http.commons.TimestampDataLoader;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.simplereport.SimpleReport;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.validation.AdvancedSignature;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.validation.reports.Reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class JsonDocumentSigner {

    private KeyStore _keyStore;

    @SuppressWarnings("unused")
    public JsonDocumentSigner() {
        //
    }

    public JsonDocumentSigner(KeyStore keyStore) {
        this();
        this._keyStore = keyStore;
    }

    @SuppressWarnings("unused")
    private PrivateKey getPrivateKey(KeyStore ks, String thumbprint,
                                     String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias = null;
        String findAlias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint.toLowerCase())) {
                    findAlias = alias;
                    break;
                }
            }
        }
        if (findAlias == null) {
            throw new CertificateException("The specified certificate cannot be found");
        }
        // get private key
        return (PrivateKey) ks.getKey(alias, password.toCharArray());
    }

    @SuppressWarnings("RedundantThrows")
    private String getAlias(KeyStore ks, String thumbprint) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint.toLowerCase())) {
                    return alias;
                }
            }
        }
        throw new CertificateException("The specified certificate cannot be found");
    }

    private OnlineTSPSource getTSPSourceByUrl(String tsaUrl) {
        OnlineTSPSource tspSource = new OnlineTSPSource(tsaUrl);
        TimestampDataLoader dataLoader = new TimestampDataLoader();
//        dataLoader.setTimeoutConnection(TIMEOUT_MS);
//        dataLoader.setTimeoutSocket(TIMEOUT_MS);
//        dataLoader.setProxyConfig(getProxyConfig());
        tspSource.setDataLoader(dataLoader);
        return tspSource;
    }

    public void sign(File inFile,
                     File outFile,
                     String thumbprint,
                     String password,
                     String timestampServer) throws UnrecoverableEntryException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {

        KeyStore ks = this._keyStore;
        // get private key
        String alias = getAlias(ks, thumbprint);
        KeyStore.ProtectionParameter entryPassword = new KeyStore.PasswordProtection(password.toCharArray());
        KSPrivateKeyEntry privateKeyEntry = new KSPrivateKeyEntry(alias, (KeyStore.PrivateKeyEntry) this._keyStore.getEntry(alias, entryPassword));

        JAdESSignatureParameters parameters = new JAdESSignatureParameters();
        // https://ec.europa.eu/digital-building-blocks/DSS/webapp-demo/doc/dss-documentation.html#_jws_serialization_type
        // Choose the level of the signature (-B, -T, -LT, -LTA).
        parameters.setSignatureLevel(SignatureLevel.JAdES_BASELINE_B);
        // Choose the type of the signature packaging (ENVELOPING, DETACHED).
        parameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
        // Choose the form of the signature (COMPACT_SERIALIZATION, JSON_SERIALIZATION, FLATTENED_JSON_SERIALIZATION)
        parameters.setJwsSerializationType(JWSSerializationType.JSON_SERIALIZATION);
        // Set the digest algorithm
        parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
        // Set the signing certificate
        CertificateToken certificateToken = privateKeyEntry.getCertificate();
        parameters.setSigningCertificate(certificateToken);
        // Set the certificate chain
        CertificateToken[] certificateChain = privateKeyEntry.getCertificateChain();
        parameters.setCertificateChain(certificateChain);

        CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
        JAdESService service = new JAdESService(commonCertificateVerifier);
        if (timestampServer != null) {
            service.setTspSource(getTSPSourceByUrl(timestampServer));
        }
        FileDocument toSignDocument = new FileDocument(inFile);
        ToBeSigned dataToSign = service.getDataToSign(toSignDocument, parameters);

        LocalKeyStoreSignatureTokenConnection signingToken = new LocalKeyStoreSignatureTokenConnection(this._keyStore, new KeyStore.PasswordProtection(password.toCharArray()));
        // This function obtains the signature value for signed information using the
        // private key and specified algorithm
        DigestAlgorithm digestAlgorithm = parameters.getDigestAlgorithm();

        SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKeyEntry);
        // We invoke the JAdESService to sign the document with the signature value obtained in
        // the previous step.
        DSSDocument signedDocument = service.signDocument(toSignDocument, parameters, signatureValue);

        FileOutputStream outputStream = new FileOutputStream(outFile, false);
        signedDocument.writeTo(outputStream);

    }

    public VerifySignatureResult verify(File inFile) throws CertificateEncodingException, NoSuchAlgorithmException, IOException {
        FileDocument signedDocument = new FileDocument(inFile);
        CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
        JWSSerializationDocumentValidator validator = new JWSSerializationDocumentValidator(signedDocument);
        validator.setCertificateVerifier(commonCertificateVerifier);
        List<AdvancedSignature> signatures = validator.getSignatures();
        VerifySignatureResult result = new VerifySignatureResult();
        result.certificates = new ArrayList<>();
        if (signatures.size() == 0) {
            result.valid = false;
            return result;
        }
        Reports reports = validator.validateDocument();
        SimpleReport report = reports.getSimpleReport();
        AdvancedSignature advancedSignature = signatures.get(0);
        List<CertificateToken> certs = advancedSignature.getCertificates();
        for (CertificateToken cert: certs) {
            result.certificates.add(new X509CertificateInfo(cert.getCertificate()));
        }
        result.signatureProperties = new SignatureProperties();
        result.signatureProperties.signingDate = advancedSignature.getSigningTime();
        result.signatureProperties.signingCertificate = result.certificates.get(0);
        result.valid = advancedSignature.getSignatureCryptographicVerification().isSignatureValid();
        return result;
    }
}
