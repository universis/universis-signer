package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.security.auth.login.FailedLoginException;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonDocumentSignHandler implements RouterNanoHTTPD.UriResponder {

    public HashMap<String,String> files = null;

    private final static List<String> SupportedFileTypes = new ArrayList<String>(){{
        add(".json");
        add(".jsonp");
    }};

    private static final Logger log = LogManager.getLogger(SignerHandler.class);

    @SuppressWarnings("unused")
    public JsonDocumentSignHandler() {
    }

    @SuppressWarnings("unused")
    public JsonDocumentSignHandler(HashMap<String,String> files) {
        this.files = files;
    }

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @SuppressWarnings("ReassignedVariable")
    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {

        SignerAppConfiguration configuration = null;
        KeyStore ks = null;
        try {
            // get user
            UsernamePasswordCredentials user = AuthHandler.getUser(ihttpSession);
            if (user == null) {
                return new ForbiddenHandler().get(uriResource, map, ihttpSession);
            }
            // get configuration
            configuration = uriResource.initParameter(SignerAppConfiguration.class);
            // parse parameters
            SignConfiguration signConfiguration;
            try {
                if (this.files == null) {
                    // parse files
                    this.files = new HashMap<>();
                    ihttpSession.parseBody(this.files);
                    //noinspection StatementWithEmptyBody
                    for(String ignored : this.files.values()) {
                        // read
                    }
                }
                // get form data
                Map<String, List<String>> params = ihttpSession.getParameters();
                List<String> tmpFile = new ArrayList<>();
                tmpFile.add(this.files.get("file"));
                params.put("tmpFile",tmpFile);
                // and set sign configuration
                signConfiguration = SignConfiguration.fromMap(params);
            } catch (InvalidPositionException e) {
                return new BadRequestHandler("Invalid signature position").get(uriResource, map, ihttpSession);
            }
            if (signConfiguration.originalFile == null) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            if (signConfiguration.thumbprint == null) {
                return new BadRequestHandler("Parameter thumbprint may not be null.").get(uriResource, map, ihttpSession);
            }
            // get file type
            String fileType = "." + FilenameUtils.getExtension(signConfiguration.originalFile).toLowerCase();
            // if file type is *.docx or *.xlsx process request
            if (SupportedFileTypes.contains(fileType)) {
                try {
                    ks = configuration.getKeyStore(user.getPassword());
                }
                catch (KeyStoreException e) {
                    log.error(ExceptionUtils.getStackTrace(e));
                    Throwable cause = e.getCause();
                    if (cause instanceof NoSuchAlgorithmException) {
                        return new NotFoundHandler(e).get(uriResource, map, ihttpSession);
                    }
                    return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
                }
                catch (IOException e) {
                    log.error(ExceptionUtils.getStackTrace(e));
                    Throwable cause = e.getCause();
                    if (cause instanceof BadPaddingException || cause instanceof FailedLoginException || cause instanceof UnrecoverableKeyException) {
                        return new UnauthorizedHandler(e).get(uriResource, map, ihttpSession);
                    }
                    return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
                }
                String contentType;
                if (".json".equals(fileType) || ".jsonp".equals(fileType)) {
                    contentType = "application/json";
                } else {
                    configuration.tryCloseKeyStore(ks);
                    return new BadRequestHandler("Unsupported document type.").get(uriResource, map, ihttpSession);
                }
                if (!this.files.containsKey("file")) {
                    configuration.tryCloseKeyStore(ks);
                    return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
                }
                String inFile = this.files.get("file");
                // get signed document
                String outFile = File.createTempFile("signed", fileType).getAbsolutePath();
                JsonDocumentSigner signer = new JsonDocumentSigner(ks);
                // call sign
                signer.sign(new File(inFile), new File(outFile),
                        signConfiguration.thumbprint,
                        user.getPassword(),
                        signConfiguration.timestampServer);
                // finalize
                File f = new File(outFile);
                if (!f.exists()) {
                    // close key store
                    configuration.tryCloseKeyStore(ks);
                    return new NotFoundHandler().get(uriResource, map, ihttpSession);
                }
                // get content disposition
                String contentDisposition = "attachment; filename=\"" + f.getName() + "\"";
                NanoHTTPD.Response res = new FileStreamHandler(outFile, contentType).get(uriResource, map, ihttpSession);
                res.addHeader("Content-Disposition", contentDisposition);
                CorsHandler.enable(res);
                // set client origin if any
                Map<String, String> headers = ihttpSession.getHeaders();
                if (headers.containsKey("origin")) {
                    res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
                }
                // close key store
                configuration.tryCloseKeyStore(ks);
                // and return
                return res;
            } else {
                return new BadRequestHandler("Unsupported document type.").get(uriResource, map, ihttpSession);
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            if (configuration != null) {
                configuration.tryCloseKeyStore(ks);
            }
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }


    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
