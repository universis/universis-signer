package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 405 Method not allowed error
 */
public class MethodNotAllowedHandler extends ErrorHandler {

    public MethodNotAllowedHandler() {
        super(NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED);
    }
}
