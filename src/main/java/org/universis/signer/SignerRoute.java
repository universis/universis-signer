package org.universis.signer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "route")
public class SignerRoute implements Serializable {
    public SignerRoute() {
    }
    @JsonProperty("plugin")
    public String plugin;
    @JsonProperty("path")
    public String path;
    @JsonProperty("className")
    public String className;

    public static Class<?> loadClass(String plugin, String className) throws ClassNotFoundException, MalformedURLException {
            File jar = new File(plugin);
            ClassLoader loader = URLClassLoader.newInstance(
                    new URL[] { jar.toURL() },
                    SignerRoute.class.getClassLoader()
            );
            return Class.forName(className, true, loader);
    }

}
