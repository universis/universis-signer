package org.universis.signer;

import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import sun.security.x509.X500Name;

import javax.security.auth.x500.X500Principal;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * A set of attributes of a X509 certificate
 */
public class X509CertificateInfo {
    public int version;
    public String subjectDN;
    public String sigAlgName;
    public String sigAlgOID;
    public String issuerDN;
    public BigInteger serialNumber;
    public Date notAfter;
    public Date notBefore;
    public boolean expired;
    public String thumbprint;
    public String commonName;

    public X509CertificateInfo(X509Certificate cert) throws CertificateEncodingException, NoSuchAlgorithmException, IOException {
        version = cert.getVersion();
        subjectDN = cert.getSubjectDN().getName();
        boolean isX509Principal = (cert.getSubjectDN() instanceof org.bouncycastle.jce.X509Principal);
        if (!isX509Principal) {
            commonName = ((X500Name)cert.getSubjectDN()).getCommonName();
        }
        sigAlgName = cert.getSigAlgName();
        sigAlgOID = cert.getSigAlgOID();
        issuerDN = cert.getIssuerDN().getName();
        serialNumber = cert.getSerialNumber();
        notAfter = cert.getNotAfter();
        notBefore = cert.getNotBefore();
        expired = !(notAfter.after(new Date()) && notBefore.before(new Date()));
        thumbprint = X509CertificateInfo.getThumbprint(cert);
    }

    public static String getThumbprint(X509Certificate cert)
            throws NoSuchAlgorithmException, CertificateEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] der = cert.getEncoded();
        md.update(der);
        byte[] digest = md.digest();
        String digestHex = DatatypeConverter.printHexBinary(digest);
        return digestHex.toLowerCase();
    }

}
