package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerifyHandler implements RouterNanoHTTPD.UriResponder {

    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String,String> files = null;

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        try {
            // parse body
            this.files = new HashMap<>();
            Map<String, String> headers = ihttpSession.getHeaders();
            String contentType = headers.get("content-type");
            if (contentType.equals("application/json")) {
                // get body as file
                HashMap<String,String> data = new HashMap<>();
                ihttpSession.parseBody(data);
                String inFile = File.createTempFile("verify", "json").getAbsolutePath();
                // write temp file with postData
                try(FileWriter writer = new FileWriter(inFile)){
                    writer.write(data.get("postData"));
                }
                this.files.put("file", inFile);
                return  new JsonDocumentVerifyHandler(this.files).post(uriResource, map, ihttpSession);
            } else {
                ihttpSession.parseBody(this.files);
                //noinspection StatementWithEmptyBody
                for(String ignored : this.files.values()) {
                    // read
                }
            }

            // get form data
            Map<String, List<String>> params = ihttpSession.getParameters();
            // get file
            if (!params.containsKey("file")) {
                return new BadRequestHandler("File parameter is missing").get(uriResource, map, ihttpSession);
            }
            String inFile = params.get("file").get(0);
            String fileType = "." + FilenameUtils.getExtension(inFile).toLowerCase();
            switch (fileType) {
                case ".json":
                case ".jsonp":
                    return  new JsonDocumentVerifyHandler(this.files).post(uriResource, map, ihttpSession);
                case ".docx":
                case ".xlsx":
                    return  new OfficeDocumentVerifyHandler(this.files).post(uriResource, map, ihttpSession);
                case ".pdf":
                    return  new PdfVerifyHandler(this.files).post(uriResource, map, ihttpSession);
                default:
                    return new BadRequestHandler("File type is not supported.").get(uriResource, map, ihttpSession);
            }



        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }


    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
