package org.universis.signer;

import java.io.Serializable;
import java.util.List;

public class VerifySignatureResult implements Serializable {

    public boolean valid = false;
    public List<X509CertificateInfo> certificates;
    public SignatureProperties signatureProperties;

}
