import org.universis.signer.JsonDocumentSigner;
import org.universis.signer.VerifySignatureResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JsonDocumentSignerTest {

    static String CERT_THUMBPRINT = "64f56456d5c28ba0bed5052927acbf6dadb8ed8f";

    @org.junit.jupiter.api.Test
    void shouldSignJSON() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableEntryException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        JsonDocumentSigner signer = new JsonDocumentSigner(ks);
        String inFile = SignerTest.class.getResource("Test.json").getPath();
        String outFile = new java.io.File( "tmp/TestSigned.json" ).getAbsolutePath();
        signer.sign(new File(inFile), new File(outFile), CERT_THUMBPRINT, "secret", null);
        // verify signature
        VerifySignatureResult result = signer.verify(new File(outFile));
        assertTrue(result.valid);
    }

    @org.junit.jupiter.api.Test
    void shouldVerifyValid() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableEntryException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(Files.newInputStream(Paths.get(keyStorePath)), "secret".toCharArray());
        JsonDocumentSigner signer = new JsonDocumentSigner(ks);
        String inFile = SignerTest.class.getResource("TestSigned.json").getPath();
        // verify signature
        VerifySignatureResult result = signer.verify(new File(inFile));
        assertTrue(result.valid);
    }

    @org.junit.jupiter.api.Test
    void shouldVerifyInvalid() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableEntryException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(Files.newInputStream(Paths.get(keyStorePath)), "secret".toCharArray());
        JsonDocumentSigner signer = new JsonDocumentSigner(ks);
        String inFile = SignerTest.class.getResource("TestSignedInvalid.json").getPath();
        // verify signature
        VerifySignatureResult result = signer.verify(new File(inFile));
        assertFalse(result.valid);
    }

    @org.junit.jupiter.api.Test
    void shouldVerifyInvalidSignature() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableEntryException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(Files.newInputStream(Paths.get(keyStorePath)), "secret".toCharArray());
        JsonDocumentSigner signer = new JsonDocumentSigner(ks);
        String inFile = SignerTest.class.getResource("TestInvalidSignature.json").getPath();
        // verify signature
        VerifySignatureResult result = signer.verify(new File(inFile));
        assertFalse(result.valid);
    }
}