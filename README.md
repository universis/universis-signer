# universis-signer

Universis pdf signing tools

![Universis Signer](./universis-signer-screenshot.png)

universis-signer is a tiny client-side application which enables pdf signing by using 
either a PKCS12 local keystore or a PKCS11 keystore of a USB token. These signing operations are part of 
[@universis/reports](https://gitlab.com/universis/reports.git) package and allow users to sign student certificates,
transcripts and any other document that requires a digital signature.

## Installation

__Important Note: Universis Signer runs as service in all operating systems (Linux, Windows or macOS) and installation needs administrative privileges to complete__

### 32-bit Linux

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-linux-x86.zip

- Extract compressed package:

      unzip universis-signer-linux-x86.zip
      mv universis-signer-linux-x86 /opt/universis-signer-linux-x86
      sudo chown -R root:root /opt/universis-signer-linux-x86 

- Install service:

      cd /opt/universis-signer-linux-x86
      sudo ./service.sh

### 64-bit Linux

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-linux-x64.zip

- Extract compressed package:

      unzip universis-signer-linux-x64.zip
      mv universis-signer-linux-x64 /opt/universis-signer-linux-x64
      sudo chown -R root:root /opt/universis-signer-linux-x64 

- Install service:

      cd /opt/universis-signer-linux-x64
      sudo ./service.sh

### 32-bit Windows

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-windows-x86.zip

- Unzip compressed package

- Move `universis-signer-windows-x86` directory to system root e.g.  `C:\`

- Install  universis signer as windows service by executing `extras/windows/install-service.bat`

Important note: This operation needs administrative privileges

or 

- Install universis signer controller as startup application by executing `extras/windows/install-app.bat`


### 64-bit Windows

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-windows-x64.zip

- Unzip compressed package

- Move `universis-signer-windows-x64` directory to system root e.g.  `C:\`

- Install  universis signer as windows service by executing `extras/windows/install-service64.bat`

Important note: This operation needs administrative privileges

or 

- Install universis signer controller as startup application by executing `extras/windows/install-app64.bat`

### macOS

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-macosx-x64.dmg

- Open `universis-signer-macosx-x64.dmg` 

- Move `UniversisSigner.app` to applications folder

- Open `UniversisSigner.app`

Important note: If you have any problem opening `UniversisSigner` at first time, right click `UniversisSigner`'s icon and click `Open`.

#### Configure proxy settings

If network access is configured by a proxy server you should set proxy settings for embedded JVM of Universis Signer application. Locate `net.properties` file under `/jre/lib` directory of your installation.

```
# /jre/lib/net.properties

# HTTP Proxy settings. proxyHost is the name of the proxy server
# (e.g. proxy.mydomain.com), proxyPort is the port number to use (default
# value is 80) and nonProxyHosts is a '|' separated list of hostnames which
# should be accessed directly, ignoring the proxy server (default value is
# localhost & 127.0.0.1).
# http.proxyHost=
# http.proxyPort=80
http.nonProxyHosts=localhost|127.*|[::1]
#
# HTTPS Proxy Settings. proxyHost is the name of the proxy server
# (e.g. proxy.mydomain.com), proxyPort is the port number to use (default
# value is 443). The HTTPS protocol handlers uses the http nonProxyHosts list.
#
# https.proxyHost=
# https.proxyPort=443
...
```

Uncomment the following lines:

```
# http.proxyHost=
# http.proxyPort=80
```

and set `http.proxyHost` and `http.proxyPort` e.g.

```
http.proxyHost=myproxy
http.proxyPort=3128
```

Do the same for `https.proxyHost` and `https.proxyPort` e.g.

```
https.proxyHost=myproxy
https.proxyPort=3128
```


## Development usage

### Start universis-signer with a local PKCS12 key store:

        java -jar universis-signer-1.0.2.jar -keyStore ./keystore.p12 -storeType PKCS12
    
where `keyStore` arguments holds the path of the local PKCS12 key store.
    
### Start universis-signer with a PKCS11 key store of a USB token:

        java -jar universis-signer-1.0.2.jar -keyStore none -storeType PKCS11 -providerArg ./eToken.cfg
    
where `storeType` argument holds key store type (PKCS11) and `providerArg` contains the path of USB token configuration e.g.

    name=eToken
    library=/usr/local/lib/libeTPkcs11.dylib
    slot=0

or any other USB token required configuration

## List key store certificates

The following request

    curl --location --request GET 'localhost:2465/keystore/certs' \
    --header 'Authorization: Basic dXNlcjpzZWNyZXQ='

returns a list of the available certificates e.g.

    [
        {
            "version": 3,
            "subjectDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "sigAlgName": "SHA256withRSA",
            "sigAlgOID": "1.2.840.113549.1.1.11",
            "issuerDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "serialNumber": 11651655752893169577,
            "notAfter": "Jun 18, 2022 9:29:12 AM",
            "notBefore": "Jun 18, 2020 9:29:12 AM",
            "expired": false,
            "thumbprint": "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
            "commonName": "Universis Test Signer"
        }
    ]
    
Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.
 

## Sign pdf document

![Signed PDF Document](./screenshot-signed-pdf.png)

The following request passes an input file and returns a signed pdf e.g.

    curl --location --request POST 'localhost:2465/sign/' \
    --header 'Authorization: Basic dXNlcjoxMjM0' \
    --form 'thumbprint=f641d66d49ba43bf911e873889d7fa0e32e26ce6' \
    --form 'position=20,10,320,120' \
    --form 'file=@/home/user/Downloads/document.pdf'

where `thumbprint` parameter is the thumbprint of the certificate that is going to used to sign pdf document,
`position` is an optional parameter of the position of the signature block and `file` contains the pdf document to be signed.

Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.

## Sign pdf and include graphic signature and reason

Universis signer supports embedding a graphic signature and a reason text while signing pdf documents:

    curl --location --request POST 'http://localhost:2465/sign' \
    --header 'Authorization: Basic dXNlcjpzZWNyZXQ=' \
    --form 'file=@"/home/user/Documents/document.pdf"' \
    --form 'thumbprint="f641d66d49ba43bf911e873889d7fa0e32e26ce6"' \
    --form 'reason="A signature reason for testing"' \
    --form 'image=@"/home/user/Documents/TestSigner.jpg"'
 
 where `image` parameter may contain the graphic signature and `reason` parameter the reason for signing document.

 ## Use multiple signatures

Use name parameter and control signature block position to sign a document with more than one signatures.

Sign document by adding name -and maybe reason-:

```
curl --location --request POST 'localhost:2465/sign' \
--header 'Authorization: Basic dXNlcjpzZWNyZXQ=' \
--form 'thumbprint=d33ac6cc6290bcfb4a23243af0ec98b4f49b2238' \
--form 'position=20,50,270,100' \
--form 'file=@/path/to/file' \
--form 'name=sender' \
--form 'reason=Sender signature'
```
Sign document again by adding another name and reason:

```
curl --location --request POST 'localhost:2465/sign' \
--header 'Authorization: Basic dXNlcjpzZWNyZXQ=' \
--form 'thumbprint=d33ac6cc6290bcfb4a23243af0ec98b4f49b2238' \
--form 'position=300,50,570,100' \
--form 'file=@/path/to/signed/file' \
--form 'name=recipient' \
--form 'reason=Recipient signature'
```

![Multiple signatures](./multiple-signatures-screenshot.png)

**Important Note**: Any signing operation which wants to sign a document with multiple signatures should take care about position parameter in order to avoid signature field overlapping.

## Sign Office OpenXML documents

Use `/sign` endpoint to sign Office OpenXML documents like *.xlsx or *,docx.

```
curl --location --request POST 'http://localhost:2465/sign' \
--header 'Authorization: Basic dXNlcjpzZWNyZXQ=' \
--form 'file=@"/tmp/students.xlsx"' \
--form 'thumbprint="d33ac6cc6290bcfb4a23243af0ec98b4f49b2238"'
``` 

which returns a signed document.

![Microsoft Excel - Show digital signatures](./excel-digital-signatures.png)

![Libre Office - show digital signatures](./libre-office-digital-signatures.png)

## Verify document signature

Use `/verify` service endpoint to verify a document. Service response is a JSON document which contains information about digital signatures found on this document.

```
curl --location --request POST 'http://localhost:2465/verify' \
--header 'Accept: application/json' \
--form 'file=@"/tmp/students_signed.xlsx"'
```

which returns and array of `VerifySignatureResult`:

```
[
    {
        "valid": true,
        "certificates": [
            {
                "version": 3,
                "subjectDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
                "sigAlgName": "SHA256withRSA",
                "sigAlgOID": "1.2.840.113549.1.1.11",
                "issuerDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
                "serialNumber": 11651655752893169577,
                "notAfter": "2022-06-18T06:29:12.000+00:00",
                "notBefore": "2020-06-18T06:29:12.000+00:00",
                "expired": false,
                "thumbprint": "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
                "commonName": "Universis Test Signer"
            }
        ],
        "signatureProperties": {
            "signingDate": "2021-01-07T05:05:28.000+00:00",
            "reason": null,
            "signingCertificate": {
                "version": 3,
                "subjectDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
                "sigAlgName": "SHA256withRSA",
                "sigAlgOID": "1.2.840.113549.1.1.11",
                "issuerDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
                "serialNumber": 11651655752893169577,
                "notAfter": "2022-06-18T06:29:12.000+00:00",
                "notBefore": "2020-06-18T06:29:12.000+00:00",
                "expired": false,
                "thumbprint": "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
                "commonName": "Universis Test Signer"
            }
        }
    }
]
```

- **VerifySignatureResult#valid**

A boolean value which indicates whether the specified digital signature is valid or not.

- **VerifySignatureResult#certificates**

An array of X509 certificates which represents the certificate chain used by this signature.

- **VerifySignatureResult#signatureProperties**

An object which holds information about digital signature. `signingDate` is the date and time when this signature was created and `signingCertificate` is the certificate used to sign this document.

 ## Build from sources
 
Clone universis-signer repository:
 
    git clone https://gitlab.com/universis/universis-signer.git
    
Run maven and build package:

    mvn package

## Create PKCS12 store

Execute the following command to create a self-signed certificate and add it to a PKCS12 keystore

    openssl req -newkey rsa:2048 -x509 -keyout key.pem -out cert.pem -days 365
    cat key.pem cert.pem > cert-bundled.pem

    openssl pkcs12 -export -in cert-bundled.pem -out keystore.p12

## Use PKCS12 store

### Linux

- Stop universis-signer.service

      sudo systemctl stop universis-signer.service

- Copy PKCS12 store to universis-signer installation directory e.g /opt/universis-signer-linux-x64 or /opt/universis-signer-linux-x86

- Modify service configuration to use PKCS12 store

      sudo nano /etc/systemd/system/universis-signer.service

      [Unit]
      Description=Universis Signer

      [Service]
      Type=simple
      ExecStart=/bin/bash -c '"/opt/universis-signer-linux-x64/jre/bin/java" -cp "/opt/universis-signer-linux-x64/universis-signer.jar" org.universis.signer.SignerService -storeType PKCS12 -keyStore "/opt/universis-signer-linux-x64/keystore.p12"'
      User=root
      Group=root
      WorkingDirectory=/opt/universis-signer-linux-x64

      [Install]
      WantedBy=multi-user.target

- And finally start service

      sudo systemctl start universis-signer.service

### Windows

- Copy keystore.p12 in installation directory e.g. C:\universis-signer-windows-x64\

- Open windows registry editor `regedit.exe`, go to `HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\UniversisSigner\Parameters\Start` key and modify `Params` value in order to include PKCS12 store.

       -storeType
       PKCS12
       -keyStore
       C:\universis-signer-windows-x64\keystore.p12

- Restart Universis Signer service (Control Panel\Administrative Tools\Services)

![Universis Signer Regedit](./windows-regedit-screenshot.png)


### macOS

- Stop org.univeris.signer agent

      sudo launchctl unload /Library/LaunchAgents/org.universis.signer.plist

- Copy PKCS12 store to universis-signer installation directory e.g /opt/universis-signer/

- Modify service configuration to use PKCS12 store

      sudo nano /Library/LaunchAgents/org.universis.signer.plist

      <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
      <plist version="1.0">
      <dict>
        <key>KeepAlive</key>
        <false/>
        <key>Label</key>
        <string>org.universis.signer</string>
        <key>OnDemand</key>
        <true/>
        <key>ProgramArguments</key>
        <array>
                <string>/opt/universis-signer/jre/bin/java</string>
                <string>-jar</string>
                <string>/opt/universis-signer/universis-signer.jar</string>
                <string>-storeType</string>
                <string>PKCS12</string>
                <string>-keyStore</string>
                <string>/opt/universis-signer/keystore.p12</string>
        </array>
        <key>RunAtLoad</key>
        <true/>
        <key>StandardErrorPath</key>
        <string>/var/log/universis-signer.log</string>
        <key>StandardOutPath</key>
        <string>/var/log/universis-signer.log</string>
        <key>WorkingDirectory</key>
        <string>/opt/universis-signer</string>
      </dict>

- And finally load agent

      sudo launchctl load -w /Library/LaunchAgents/org.universis.signer.plist



